﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUnspscService.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   The interface for the unspsc service
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc
{
    using System.Collections.Generic;
    using Audit;

    /// <summary>
    /// The interface for the unspsc service
    /// </summary>
    public interface IUnspscService
    {
        /// <summary>
        /// Gets the available versions.
        /// </summary>
        /// <returns>IList of version numbers</returns>
        IList<double> GetVersions();

        /// <summary>
        /// Gets the segments.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>IList of IUnspscElement objects</returns>
        IList<IUnspscElement> GetSegments(double version);

        /// <summary>
        /// Gets the families.
        /// </summary>
        /// <param name="segmentCode">The segment code.</param>
        /// <param name="version">The version.</param>
        /// <returns>IList of IUnspscElement objects</returns>
        IList<IUnspscElement> GetFamilies(UnspscCode segmentCode, double version);

        /// <summary>
        /// Gets the classes.
        /// </summary>
        /// <param name="familyCode">The family code.</param>
        /// <param name="version">The version.</param>
        /// <returns>IList of IUnspscElement objects</returns>
        IList<IUnspscElement> GetClasses(UnspscCode familyCode, double version);

        /// <summary>
        /// Gets the commodities.
        /// </summary>
        /// <param name="classCode">The class code.</param>
        /// <param name="version">The version.</param>
        /// <returns>IList of IUnspscElement objects</returns>
        IList<IUnspscElement> GetCommodities(UnspscCode classCode, double version);

        /// <summary>
        /// Gets the commodities.
        /// </summary>
        /// <param name="unspscCode">The unspsc code.</param>
        /// <param name="version">The version.</param>
        /// <returns>IList of IUnspscElement objects</returns>
        IUnspscElement GetUnspscElement(UnspscCode unspscCode, double version);

        /// <summary>
        /// Searches the text.
        /// </summary>
        /// <param name="keywords">The keywords.</param>
        /// <param name="version">The version.</param>
        /// <returns>IList of IUnspscElement objects</returns>
        IList<IUnspscElement> SearchText(string[] keywords, double version);

        /// <summary>
        /// Gets the audit items.
        /// </summary>
        /// <param name="code">The unspsc code.</param>
        /// <returns>IList of IAuditItem</returns>
        IList<IAuditItem> GetAuditItems(UnspscCode code);
    }
}
