﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnspscService.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Defines the UnspscService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Audit;
    using Unspsc.Data;

    /// <summary>
    /// an instance of the unspsc service
    /// </summary>
    public class UnspscService : IUnspscService
    {
        /// <summary>
        /// internal repository for the segments
        /// </summary>
        private readonly IUnspscRepository unspscRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnspscService"/> class.
        /// </summary>
        /// <param name="unspscRepository">The unspsc repository.</param>
        /// <exception cref="ArgumentNullException"/>
        public UnspscService(IUnspscRepository unspscRepository)
        {
            if (unspscRepository == null)
                throw new ArgumentNullException("unspscRepository", "Repository cannot be null");

            this.unspscRepository = unspscRepository;
        }

        /// <summary>
        /// Gets the available versions.
        /// </summary>
        /// <returns>IList of version numbers</returns>
        public IList<double> GetVersions()
        {
            return unspscRepository.GetVersions().ToList();
        }

        /// <summary>
        /// Gets the segments.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>IList of IUnspscElement objects</returns>
        public IList<IUnspscElement> GetSegments(double version)
        {
            return unspscRepository.GetSegments(version).ToList();
        }

        /// <summary>
        /// Gets the families.
        /// </summary>
        /// <param name="segmentCode">The segment code.</param>
        /// <param name="version">The version.</param>
        /// <returns>IList of IUnspscElement objects</returns>
        public IList<IUnspscElement> GetFamilies(UnspscCode segmentCode, double version)
        {
            return unspscRepository.GetFamilies(segmentCode, version).ToList();
        }

        /// <summary>
        /// Gets the classes.
        /// </summary>
        /// <param name="familyCode">The family code.</param>
        /// <param name="version">The version.</param>
        /// <returns>IList of IUnspscElement objects</returns>
        public IList<IUnspscElement> GetClasses(UnspscCode familyCode, double version)
        {
            return unspscRepository.GetClasses(familyCode, version).ToList();
        }

        /// <summary>
        /// Gets the commodities.
        /// </summary>
        /// <param name="classCode">The class code.</param>
        /// <param name="version">The version.</param>
        /// <returns>IList of IUnspscElement objects</returns>
        public IList<IUnspscElement> GetCommodities(UnspscCode classCode, double version)
        {
            return unspscRepository.GetCommodities(classCode, version).ToList();
        }

        /// <summary>
        /// Gets the commodities.
        /// </summary>
        /// <param name="unspscCode">The unspsc code.</param>
        /// <param name="version">The version.</param>
        /// <returns>IList of IUnspscElement objects</returns>
        public IUnspscElement GetUnspscElement(UnspscCode unspscCode, double version)
        {
            return unspscRepository.GetUnspscElement(unspscCode, version);
        }

        /// <summary>
        /// Searches the text.
        /// </summary>
        /// <param name="keywords">The keywords.</param>
        /// <param name="version">The version.</param>
        /// <returns>IList of IUnspscElement objects</returns>
        public IList<IUnspscElement> SearchText(string[] keywords, double version)
        {
            return unspscRepository.SearchText(keywords, version).ToList();
        }

        /// <summary>
        /// Gets the audit items.
        /// </summary>
        /// <param name="code">The unspsc code.</param>
        /// <returns>IList of IAuditItem</returns>
        public IList<IAuditItem> GetAuditItems(UnspscCode code)
        {
            return unspscRepository.GetAuditLog(code).ToList();
        }
    }
}