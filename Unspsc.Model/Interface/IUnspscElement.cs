// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUnspscElement.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Defines the IUnspscElement type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc
{
    using System;

    public interface IUnspscElement : IFormattable, IEquatable<IUnspscElement>, IComparable<UnspscCode> 
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key of the unspsc element used for auditing changes.</value>
        double Key { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code for the element.</value>
        UnspscCode Code { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title of the element.</value>
        string Title { get; set; }

        /// <summary>
        /// Gets or sets the definition.
        /// </summary>
        /// <value>The definition.</value>
        string Definition { get; set; }

        /// <summary>
        /// Gets or sets the code version.
        /// </summary>
        /// <value>The code version.</value>
        double Version { get; set; }
    }
}