// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAuditItem.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Defines the IAuditItem type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc.Audit
{
    using System;

    public interface IAuditItem
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id fo the change.</value>
        double? Id { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The unspsc code.</value>
        UnspscCode? Code { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        double Version { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        string Title { get; set; }

        /// <summary>
        /// Gets or sets the audit change type.
        /// </summary>
        /// <value>The change type.</value>
        ChangeType Type { get; set; }

        /// <summary>
        /// Gets or sets the date which the change was effective from.
        /// </summary>
        /// <value>The effective from.</value>
        DateTime EffectiveFrom { get; set; }

    }
}