// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnspscElement.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Defines the UnspscBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc
{
    using System;

    /// <summary>
    /// Class to store a unspsc element, eg segment, family, class, commodity
    /// </summary>
    [Serializable]
    public class UnspscElement : IUnspscElement
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key of the unspsc element used for auditing changes.</value>
        public double Key { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code for the element.</value>
        public UnspscCode Code { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title of the element.</value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the definition.
        /// </summary>
        /// <value>The definition.</value>
        public string Definition { get; set; }

        /// <summary>
        /// Gets or sets the code version.
        /// </summary>
        /// <value>The code version.</value>
        public double Version { get; set; }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public override bool Equals(object obj)
        {
            if (obj is IUnspscElement)
            {
                var compareTo = (IUnspscElement)obj;
                return compareTo.Code == Code;
            }

            return base.Equals(obj);
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        public bool Equals(IUnspscElement other)
        {
            if (other == null)
                return false;

            return Code.Equals(other);
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has the following meanings:
        /// Value
        /// Meaning
        /// Less than zero
        /// This object is less than the <paramref name="other"/> parameter.
        /// Zero
        /// This object is equal to <paramref name="other"/>.
        /// Greater than zero
        /// This object is greater than <paramref name="other"/>.
        /// </returns>
        /// <exception cref="NotImplementedException" />
        public int CompareTo(UnspscCode other)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{0} - {1}", Code, Title);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="formatProvider">The format provider.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public string ToString(string format, System.IFormatProvider formatProvider)
        {
            return Code.ToString(format, formatProvider);
        }
    }
}