// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuditItem.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Defines the AuditItem type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc.Audit
{
    using System;

    public class AuditItem : IAuditItem
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id fo the change.</value>
        public double? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The unspsc code.</value>
        public UnspscCode? Code
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        public double Version
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        public string Title
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the audit change type.
        /// </summary>
        /// <value>The change type.</value>
        public ChangeType Type
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the date which the change was effective from.
        /// </summary>
        /// <value>The effective from.</value>
        public DateTime EffectiveFrom
        {
            get;
            set;
        }
    }
}