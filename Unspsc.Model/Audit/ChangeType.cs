// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChangeType.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Defines the ChangeType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc.Audit
{
    public enum ChangeType
    {
        Add    = 0,
        Edit   = 1,
        Delete = 2,
        Move
    }
}