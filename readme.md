# UNSPSC

The United Nations Standard Products and Services Code **(UNSPSC)** API is developed in C# using the .Net 3.5 Framework to assist in integrating the unspsc codes into business systems.

The United Nations Standard Products and Services Code� (UNSPSC�) provides an open, global multi-sector standard for efficient, accurate classification of products and services. The UNSPSC offers a single global classification system that can be used for:

Company-wide visibility of spend analysis
Cost-effective procurement optimization
Full exploitation of electronic commerce capabilities

For more information visit the [UNSPSC Website](http://www.unspsc.org/)