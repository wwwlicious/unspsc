﻿namespace Unspsc.Windows
{
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Audit;

    public partial class AuditHistory : Form
    {
        public AuditHistory(IList<IAuditItem> items)
        {
            InitializeComponent();

            dataGridAudit.DataSource = items;
        }
    }
}
