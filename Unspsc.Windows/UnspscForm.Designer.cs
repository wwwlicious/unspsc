﻿namespace Unspsc.Windows
{
    using System;

    partial class UnspscForm
	{
	    /// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            var dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(UnspscForm));
            this.cmbBoxSegments = new System.Windows.Forms.ComboBox();
            this.cmbBoxFamilies = new System.Windows.Forms.ComboBox();
            this.cmbBoxClasses = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.dataGridCommodities = new System.Windows.Forms.DataGridView();
            this.Key = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Definition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbVersions = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCommodities)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbBoxSegments
            // 
            this.cmbBoxSegments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBoxSegments.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxSegments.FormattingEnabled = true;
            this.cmbBoxSegments.Location = new System.Drawing.Point(20, 57);
            this.cmbBoxSegments.Margin = new System.Windows.Forms.Padding(5);
            this.cmbBoxSegments.Name = "cmbBoxSegments";
            this.cmbBoxSegments.Size = new System.Drawing.Size(1025, 30);
            this.cmbBoxSegments.TabIndex = 0;
            this.cmbBoxSegments.SelectedIndexChanged += new System.EventHandler(this.cmbBoxSegments_SelectedIndexChanged);
            // 
            // cmbBoxFamilies
            // 
            this.cmbBoxFamilies.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBoxFamilies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxFamilies.FormattingEnabled = true;
            this.cmbBoxFamilies.Location = new System.Drawing.Point(20, 103);
            this.cmbBoxFamilies.Margin = new System.Windows.Forms.Padding(5);
            this.cmbBoxFamilies.Name = "cmbBoxFamilies";
            this.cmbBoxFamilies.Size = new System.Drawing.Size(1025, 30);
            this.cmbBoxFamilies.TabIndex = 1;
            this.cmbBoxFamilies.SelectedIndexChanged += new System.EventHandler(this.cmbBoxFamilies_SelectedIndexChanged);
            // 
            // cmbBoxClasses
            // 
            this.cmbBoxClasses.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBoxClasses.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxClasses.FormattingEnabled = true;
            this.cmbBoxClasses.Location = new System.Drawing.Point(20, 149);
            this.cmbBoxClasses.Margin = new System.Windows.Forms.Padding(5);
            this.cmbBoxClasses.Name = "cmbBoxClasses";
            this.cmbBoxClasses.Size = new System.Drawing.Size(1025, 30);
            this.cmbBoxClasses.TabIndex = 2;
            this.cmbBoxClasses.SelectedIndexChanged += new System.EventHandler(this.cmbBoxClasses_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 205);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(290, 22);
            this.label1.TabIndex = 4;
            this.label1.Text = "Search the title/description";
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Location = new System.Drawing.Point(322, 200);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(5);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(582, 30);
            this.txtSearch.TabIndex = 5;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // dataGridCommodities
            // 
            this.dataGridCommodities.AllowUserToAddRows = false;
            this.dataGridCommodities.AllowUserToDeleteRows = false;
            this.dataGridCommodities.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridCommodities.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridCommodities.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dataGridCommodities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridCommodities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Key,
            this.Code,
            this.Title,
            this.Definition});
            this.dataGridCommodities.Location = new System.Drawing.Point(20, 252);
            this.dataGridCommodities.Margin = new System.Windows.Forms.Padding(5);
            this.dataGridCommodities.Name = "dataGridCommodities";
            this.dataGridCommodities.ReadOnly = true;
            this.dataGridCommodities.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridCommodities.Size = new System.Drawing.Size(1028, 452);
            this.dataGridCommodities.TabIndex = 6;
            this.dataGridCommodities.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridCommodities_CellContentClick);
            // 
            // Key
            // 
            this.Key.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Key.DataPropertyName = "Key";
            this.Key.HeaderText = "Key";
            this.Key.Name = "Key";
            this.Key.ReadOnly = true;
            this.Key.Width = 65;
            // 
            // Code
            // 
            this.Code.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Code.DataPropertyName = "Code";
            this.Code.HeaderText = "Code";
            this.Code.Name = "Code";
            this.Code.ReadOnly = true;
            this.Code.Width = 75;
            // 
            // Title
            // 
            this.Title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Title.DataPropertyName = "Title";
            this.Title.HeaderText = "Title";
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            this.Title.Width = 85;
            // 
            // Definition
            // 
            this.Definition.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Definition.DataPropertyName = "Definition";
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Definition.DefaultCellStyle = dataGridViewCellStyle1;
            this.Definition.HeaderText = "Definition";
            this.Definition.Name = "Definition";
            this.Definition.ReadOnly = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(917, 196);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(125, 39);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 19);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(330, 22);
            this.label2.TabIndex = 8;
            this.label2.Text = "Select the unspsc version to use";
            // 
            // cmbVersions
            // 
            this.cmbVersions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVersions.FormattingEnabled = true;
            this.cmbVersions.Location = new System.Drawing.Point(360, 16);
            this.cmbVersions.Name = "cmbVersions";
            this.cmbVersions.Size = new System.Drawing.Size(210, 30);
            this.cmbVersions.TabIndex = 9;
            this.cmbVersions.SelectedIndexChanged += new System.EventHandler(this.cmbVersions_SelectedIndexChanged);
            // 
            // UnspscForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1068, 724);
            this.Controls.Add(this.cmbVersions);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.dataGridCommodities);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbBoxClasses);
            this.Controls.Add(this.cmbBoxFamilies);
            this.Controls.Add(this.cmbBoxSegments);
            this.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "UnspscForm";
            this.Text = "Lookup UNSPSC";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCommodities)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox cmbBoxSegments;
		private System.Windows.Forms.ComboBox cmbBoxFamilies;
        private System.Windows.Forms.ComboBox cmbBoxClasses;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView dataGridCommodities;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn Key;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Definition;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbVersions;
	}
}

