﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnspscForm.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Defines the UnspscForm type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;
    using Unspsc.Data;
    using Unspsc.Services;

    public partial class UnspscForm : Form
    {
        private const string ConnString = @"Data Source=.\SQLEXPRESS;AttachDbFilename=|DataDirectory|\Db\Unspsc.mdf;Integrated Security=True;User Instance=True";

        private readonly UnspscService service;

        private IList<IUnspscElement> segmentList;
        private IList<IUnspscElement> commodityList;

        private double version;

        public UnspscForm()
        {
            IList<double> versionList = null;
            InitializeComponent();

            // Load the service
            var data = new SqlUnspscRepository(ConnString);
            service = new UnspscService(data);

            // Set the default version, this loads all the other comboboxes
            this.service.GetVersions();
            version = versionList.First();
            this.cmbVersions.DataSource = versionList;
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the cmbBoxSegments control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void cmbBoxSegments_SelectedIndexChanged(object sender, EventArgs e)
        {
            var segment = this.cmbBoxSegments.SelectedItem as IUnspscElement;
            this.cmbBoxFamilies.DataSource = service.GetFamilies(segment.Code, version).ToList();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the cmbBoxFamilies control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void cmbBoxFamilies_SelectedIndexChanged(object sender, EventArgs e)
        {
            var family = this.cmbBoxFamilies.SelectedItem as IUnspscElement;
            this.cmbBoxClasses.DataSource = service.GetClasses(family.Code, version).ToList();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the cmbBoxClasses control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void cmbBoxClasses_SelectedIndexChanged(object sender, EventArgs e)
        {
            var unspscClass = this.cmbBoxClasses.SelectedItem as IUnspscElement;
            commodityList = service.GetCommodities(unspscClass.Code, version).ToList();

            // add the selected segment, family and class to the listing
            commodityList.Insert(0, this.cmbBoxSegments.SelectedItem as IUnspscElement);
            commodityList.Insert(1, this.cmbBoxFamilies.SelectedItem as IUnspscElement);
            commodityList.Insert(2, this.cmbBoxClasses.SelectedItem as IUnspscElement);
            SetGridSource();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the cmbVersions control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void cmbVersions_SelectedIndexChanged(object sender, EventArgs e)
        {
            version = (double)this.cmbVersions.SelectedItem;
            segmentList = service.GetSegments(version);
            cmbBoxSegments.DataSource = this.segmentList;
        }

        /// <summary>
        /// Handles the KeyDown event of the txtSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
        protected void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                this.btnSearch_Click(this, e);
        }

        /// <summary>
        /// Handles the CellContentClick event of the dataGridCommodities control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.DataGridViewCellEventArgs"/> instance containing the event data.</param>
        protected void dataGridCommodities_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // Load any audit entries here
            var element = dataGridCommodities.CurrentRow.DataBoundItem as IUnspscElement;
            if (element == null) return;

            var items = this.service.GetAuditItems(element.Code);

            if (!items.Any())
            {
                MessageBox.Show("No audit history");
                return;
            }

            var form = new AuditHistory(items);
            form.ShowDialog();
        }

        /// <summary>
        /// Handles the Click event of the btnSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var keywords = this.txtSearch.Text.Trim().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            this.commodityList = this.service.SearchText(keywords,version);
            this.SetGridSource();
        }

        /// <summary>
        /// Sets the grid source.
        /// </summary>
        private void SetGridSource()
        {
            dataGridCommodities.DataSource = commodityList;
        }
    }
}