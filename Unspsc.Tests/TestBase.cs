﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestBase.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Summary description for TestBase
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc.Tests
{
    using System;
    using Unspsc.Data;
    using Unspsc.Services;

    /// <summary>
    /// Summary description for TestBase
    /// </summary>
    public class TestBase : IDisposable
    {
        internal IUnspscService unspscService;

        internal IUnspscRepository unspscRepository;

        private const string connString = @"Data Source=.\SQLEXPRESS;AttachDbFilename=|DataDirectory|\Db\Unspsc.mdf;Integrated Security=True;User Instance=True";

        public TestBase()
        {
            unspscRepository = new TestUnspscRepository();
            unspscService = new UnspscService(unspscRepository);
        }

        public void Dispose()
        {
            unspscRepository = null;
            unspscService = null;
        }
    }
}

