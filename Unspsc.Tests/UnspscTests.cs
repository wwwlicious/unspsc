﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnspscTests.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Summary description for UnitTest1
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc.Tests
{
    using System.Linq;
    using Xunit;

    /// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	public class UnspscTests : TestBase
	{
	    private double version = 1;

		[Fact]
		public void UnspscRepositoryTestRepositorySegmentsIsNotNull()
		{
            Assert.NotNull(unspscRepository.GetSegments(version));
		}

		[Fact]
		public void UnspscRepositoryTestRepositorySegmentsReturns5()
		{
            Assert.Equal(5, unspscRepository.GetSegments(version).Count());
		}

		[Fact]
		public void UnspscServiceCanGetSegmentsFromService()
		{
            var list = unspscService.GetSegments(version);
			Assert.True(list.Count > 0);
		}

	    [Fact]
		public void UnspscRepositoryTestRepositoryFamiliesIsNotNull()
		{
            var parent = unspscRepository.GetSegments(version).First();
            Assert.NotNull(parent);
            Assert.NotNull(unspscRepository.GetFamilies(parent.Code));
		}

		[Fact] 
		public void UnspscRepositoryTestRepositoryFamiliesReturns25()
		{
            var parent = unspscRepository.GetSegments(version).First();
            Assert.NotNull(parent);
            Assert.Equal(5, unspscRepository.GetFamilies(parent.Code, version).Count());
		}

		[Fact]
		public void UnspscServiceCanGetFamiliesFromService()
		{
            var list = unspscService.GetFamilies(10000000d, version);
			Assert.True(list.Count > 0);
		}

	    [Fact]
		public void UnspscRepositoryTestRepositoryClassesIsNotNull()
		{
            var parent = unspscRepository.GetSegments(version).First();
            Assert.NotNull(parent);
            var parent2 = unspscRepository.GetFamilies(parent.Code, version).First();
            Assert.NotNull(parent2);
            Assert.NotNull(unspscRepository.GetClasses(parent2.Code, version));
		}

		[Fact]
		public void UnspscRepositoryTestRepositoryClassesReturns125()
		{
            var parent = unspscRepository.GetSegments(version).First();
            Assert.NotNull(parent);
            var parent2 = unspscRepository.GetFamilies(parent.Code, version).First();
            Assert.NotNull(parent2);
            Assert.Equal(5, unspscRepository.GetClasses(parent2.Code, version).Count());
		}

		[Fact]
		public void UnspscServiceCanGetClassesFromService()
		{
            var list = unspscService.GetClasses(10200000d, version);
			Assert.True(list.Count > 0);
		}

	    [Fact]
		public void UnspscRepositoryTestRepositoryCommoditiesIsNotNull()
		{
            var parent = unspscRepository.GetSegments(version).First();
            Assert.NotNull(parent);
            var parent2 = unspscRepository.GetFamilies(parent.Code, version).First();
            Assert.NotNull(parent2);
            var parent3 = unspscRepository.GetClasses(parent2.Code, version).First();
            Assert.NotNull(parent3);
            Assert.NotNull(unspscRepository.GetCommodities(parent3.Code, version));
		}

		[Fact]
		public void UnspscRepositoryTestRepositoryCommoditiesReturns625()
		{
            var parent = unspscRepository.GetSegments(version).First();
            Assert.NotNull(parent);
            var parent2 = unspscRepository.GetFamilies(parent.Code, version).First();
            Assert.NotNull(parent2);
            var parent3 = unspscRepository.GetClasses(parent2.Code, version).First();
            Assert.NotNull(parent3);
            Assert.Equal(5, unspscRepository.GetCommodities(parent3.Code, version).Count());
		}

		[Fact]
		public void UnspscServiceCanGetCommoditiesFromService()
		{
            var list = unspscService.GetCommodities(10201500d, version);
			Assert.True(list.Count > 0);
		}
	}
}
