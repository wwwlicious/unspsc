﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnspscCodeTests.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Summary description for UnspscCodeTests
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc.Tests
{
    using System;
    using System.Globalization;
    using Xunit;


    /// <summary>
    /// Summary description for UnspscCodeTests
    /// </summary>
    public class UnspscCodeTests
    {
        [Fact]
        public void UnspscCodeCorrectCodePopulatesFields()
        {
            UnspscCode code = 12345678;

            Assert.Equal(12000000, code.Segment);
            Assert.Equal(false, code.IsSegment);

            Assert.Equal(12340000, code.Family);
            Assert.Equal(false, code.IsFamily);

            Assert.Equal(12345600, code.Class);
            Assert.Equal(false, code.IsClass);

            Assert.Equal(12345678, code.Commodity);
            Assert.Equal(true, code.IsCommodity);
        }

        [Fact]
        public void UnspscCodeIncorrectMinCodeThrowsException()
        {
            Assert.Throws<ArgumentException>(() => { UnspscCode code = UnspscCode.MinValue - 1; });
        }

        [Fact]
        public void UnspscCodeIncorrectMaxCodeThrowsException()
        {
            Assert.Throws<ArgumentException>(() => { UnspscCode code = UnspscCode.MaxValue + 1; });
        }

        [Fact]
        public void UnspscCodeMinValueCanBeInitialised()
        {
            UnspscCode code = UnspscCode.MinValue;
            Assert.Equal(UnspscCode.MinValue, (double)code);
        }

        [Fact]
        public void UnspscCodeMaxValueCanBeInitialised()
        {
            UnspscCode code = UnspscCode.MaxValue;
            Assert.Equal(UnspscCode.MaxValue, (double)code);
        }

        [Fact]
        public void UnspscCodeEqualsOperatorMatchesIdenticalCodes()
        {
            UnspscCode code = UnspscCode.MinValue;
            UnspscCode code2 = UnspscCode.MinValue;
            Assert.True(code == code2);
        }

        [Fact]
        public void UnspscCodeEqualsOperatorFailsDifferentCodes()
        {
            UnspscCode code = UnspscCode.MinValue;
            UnspscCode code2 = UnspscCode.MaxValue;
            Assert.False(code == code2);
        }

        [Fact]
        public void UnspscCodeNotEqualsOperatorFailsWithIdenticalCodes()
        {
            UnspscCode code = UnspscCode.MinValue;
            UnspscCode code2 = UnspscCode.MinValue;
            Assert.False(code != code2);
        }

        [Fact]
        public void UnspscCodeNotEqualsOperatorPassesDifferentCodes()
        {
            UnspscCode code = UnspscCode.MinValue;
            UnspscCode code2 = UnspscCode.MaxValue;
            Assert.True(code != code2);
        }

        [Fact]
        public void UnspscCodeGetTypeCodeTest()
        {
            var target = new UnspscCode(); 
            Object expected = TypeCode.Double; 
            var actual = target.GetTypeCode();
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void UnspscCodeCompareToTest()
        {
            UnspscCode target = UnspscCode.MinValue;
            UnspscCode other = UnspscCode.MinValue;
            var expected = 0;
            var actual = target.CompareTo(other);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void UnspscCodeObjectEqualsTest()
        {
            UnspscCode target = UnspscCode.MinValue;
            UnspscCode obj = UnspscCode.MinValue;
            var expected = true;
            bool actual;
            actual = target.Equals(obj);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void UnspscCodeEqualsTest()
        {
            UnspscCode target = UnspscCode.MinValue;
            UnspscCode other = UnspscCode.MinValue;
            var expected = true;
            var actual = target.Equals(other);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void UnspscCodeGetHashCodeTest()
        {
            UnspscCode target = UnspscCode.MinValue;
            var expected = 1097011920;
            var actual = target.GetHashCode();
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void UnspscCodeImplicitConversionToDecimal()
        {
            UnspscCode value = UnspscCode.MinValue;
            decimal actual = value;
            decimal expected = 10000000;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void UnspscCodeImplicitConversionToLong()
        {
            UnspscCode value = UnspscCode.MinValue;
            long actual = value;
            long expected = 10000000;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void UnspscCodeImplicitConversionToIntThrowsException()
        {
            var number = 1;
            Assert.Throws<ArgumentException>(() => { UnspscCode code = number; });
        }

        [Fact]
        public void UnspscCodeImplicitConversionToShortThrowsException()
        {
            short number = 1;
            Assert.Throws<ArgumentException>(() => { UnspscCode code = number; });
        }

        [Fact]
        public void UnspscCodeExplicitConversionToBooleanThrowsException()
        {
            UnspscCode code = UnspscCode.MinValue;
            Assert.Throws<NotSupportedException>(() => code.ToBoolean(null));
        }

        [Fact]
        public void UnspscCodeExplicitConversionToByteThrowsException()
        {
            UnspscCode code = UnspscCode.MinValue;
            Assert.Throws<NotSupportedException>(() => code.ToByte(null));
        }

        [Fact]
        public void UnspscCodeExplicitConversionToCharThrowsException()
        {
            UnspscCode code = UnspscCode.MinValue;
            Assert.Throws<NotSupportedException>(() => code.ToChar(null));
        }

        [Fact]
        public void UnspscCodeExplicitConversionToDateTimeThrowsException()
        {
            UnspscCode code = UnspscCode.MinValue;
            Assert.Throws<NotSupportedException>(() => code.ToDateTime(null));
        }

        [Fact]
        public void UnspscCodeExplicitConversionToInt16ThrowsException()
        {
            UnspscCode code = UnspscCode.MinValue;
            Assert.Throws<NotSupportedException>(() => code.ToInt16(null));
        }

        [Fact]
        public void UnspscCodeExplicitConversionToInt32ThrowsException()
        {
            UnspscCode code = UnspscCode.MinValue;
            Assert.Throws<NotSupportedException>(() => code.ToInt32(null));
        }

        [Fact]
        public void UnspscCodeExplicitConversionToSByteThrowsException()
        {
            UnspscCode code = UnspscCode.MinValue;
            Assert.Throws<NotSupportedException>(() => code.ToSByte(null));
        }

        [Fact]
        public void UnspscCodeExplicitConversionToSingleThrowsException()
        {
            UnspscCode code = UnspscCode.MinValue;
            Assert.Throws<NotSupportedException>(() => code.ToSingle(null));
        }

        [Fact]
        public void UnspscCodeExplicitConversionToUInt16ThrowsException()
        {
            UnspscCode code = UnspscCode.MinValue;
            Assert.Throws<NotSupportedException>(() => code.ToUInt16(null));
        }

        [Fact]
        public void UnspscCodeExplicitConversionToUInt32ThrowsException()
        {
            UnspscCode code = UnspscCode.MinValue;
            Assert.Throws<NotSupportedException>(() => code.ToUInt32(null));
        }

        [Fact]
        public void UnspscCodeExplicitConversionToDecimal()
        {
            UnspscCode code = UnspscCode.MinValue;
            Assert.Equal(code, code.ToDecimal(null));
        }

        [Fact]
        public void UnspscCodeToString()
        {
            UnspscCode code = UnspscCode.MinValue;
            var test = code.ToString(CultureInfo.InvariantCulture);
            Assert.True(test == "10-00-00-00");
        }
    }
}