﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnspscFilters.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Contains the UnspscFilters class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc.Data
{
    using System.Linq;

    /// <summary>
    /// Contains the linq filter extension methods
    /// </summary>
    public static class UnspscFilters
    {
        /// <summary>
        /// Filters the families by segment
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="segment">The segment.</param>
        /// <returns>IQueryable of IUnspscElement objects</returns>
        public static IQueryable<IUnspscElement> WithSegmentCode(this IQueryable<IUnspscElement> qry, UnspscCode segment)
        {
            UnspscCode segmentEnd = segment + 1000000d;
            var families = qry.Where(s => s.Code > segment && s.Code < segmentEnd);
            return families;
        }

        /// <summary>
        /// Filters the classes by family
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="familyId">The family id.</param>
        /// <returns>IQueryable of IUnspscElement objects</returns>
        public static IQueryable<IUnspscElement> WithFamilyCode(this IQueryable<IUnspscElement> qry, UnspscCode familyId)
        {
            UnspscCode familyEnd = familyId + 100000d;
            var classes = qry.Where(f => f.Code > familyId && f.Code < familyEnd);
            return classes;
        }

        /// <summary>
        /// Filters the commodities by class
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="classId">The class id.</param>
        /// <returns>IQueryable of IUnspscElement objects</returns>
        public static IQueryable<IUnspscElement> WithClassCode(this IQueryable<IUnspscElement> qry, UnspscCode classId)
        {
            UnspscCode classEnd = classId + 1000d;
            var commodities = qry.Where(c => c.Code > classId && c.Code < classEnd);
            return commodities;
        }

        /// <summary>
        /// Filters a unspsc type by code
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="unspscCode">The unspsc code.</param>
        /// <param name="version">The version.</param>
        /// <returns>an IUnspscElement object</returns>
        public static IUnspscElement WithUnspscCode(this IQueryable<IUnspscElement> qry, UnspscCode unspscCode, double version)
        {
            var code = qry.SingleOrDefault(c => c.Code == unspscCode && c.Version == version);
            return code;
        }

        /// <summary>
        /// Filters the IUnspscElements by version
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of IUnspscElement objects</returns>
        public static IQueryable<IUnspscElement> WithVersion(this IQueryable<IUnspscElement> qry, double version)
        {
            return qry.Where(c => c.Version == version);
        }
    }
}