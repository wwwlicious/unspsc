﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnspscElementSqlToPoco.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Defines the UnspscSegmentSqlToPoco type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc.Data.DataAccess.Sql
{
    using System;

    using Audit;

    internal static class UnspscElementSqlToPoco
    {
        /// <summary>
        /// Converts a commodity to an IUnspscElement
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>A IUnspscElement object</returns>
        internal static IUnspscElement SqlToPoco(this Commodity source)
        {
            return new UnspscElement
                   {
                       Key = (double)source.Key,
                       Code = (double)source.CommodityCode,
                       Title = source.CommodityTitle,
                       Definition = source.Definition,
                       Version = (double)source.Version,
                   };
        }

        /// <summary>
        /// Converts a Class to an IUnspscElement
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>A IUnspscElement object</returns>
        internal static IUnspscElement SqlToPoco(this Class source)
        {
            return new UnspscElement
            {
                Key = (double)source.Key,
                Code = (double)source.Code,
                Title = source.Title,
                Definition = source.Definition,
                Version = (double)source.Version,
            };
        }

        /// <summary>
        /// Converts a Family to an IUnspscElement
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>A IUnspscElement object</returns>
        internal static IUnspscElement SqlToPoco(this Family source)
        {
            return new UnspscElement
            {
                Key = (double)source.Key,
                Code = (double)source.Code,
                Title = source.Title,
                Definition = source.Definition,
                Version = (double)source.Version,
            };
        }

        /// <summary>
        /// Converts a Segment to an IUnspscElement
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>A IUnspscElement object</returns>
        internal static IUnspscElement SqlToPoco(this Segment source)
        {
            return new UnspscElement
            {
                Key = (double)source.Key,
                Code = (double)source.Code,
                Title = source.Title,
                Definition = source.Definition,
                Version = (double)source.Version,
            };
        }

        /// <summary>
        /// Converts a commodity to an IUnspscElement
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>IAuditItem object</returns>
        internal static IAuditItem SqlToPoco(this AuditLog source)
        {
            return new AuditItem
                   {
                           Code = source.changed_code.HasValue ? (double)source.changed_code.Value : new UnspscCode?(),
                           EffectiveFrom = source.effective_date,
                           Id = source.effective_id.HasValue ? (double)source.effective_id.Value : new double?(),
                           Title = source.changed_title,
                           Type = (ChangeType)Enum.Parse(typeof(ChangeType), source.change_type, true),
                           Version = (double)source.changed_version
                   };
        }
    }
}
