/****** Object:  ForeignKey [FK_AuditLog_Classes]    Script Date: 07/13/2009 20:12:11 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuditLog_Classes]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog] DROP CONSTRAINT [FK_AuditLog_Classes]
GO
/****** Object:  ForeignKey [FK_AuditLog_Commodities]    Script Date: 07/13/2009 20:12:11 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuditLog_Commodities]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog] DROP CONSTRAINT [FK_AuditLog_Commodities]
GO
/****** Object:  ForeignKey [FK_AuditLog_Families]    Script Date: 07/13/2009 20:12:11 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuditLog_Families]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog] DROP CONSTRAINT [FK_AuditLog_Families]
GO
/****** Object:  ForeignKey [FK_AuditLog_Segments]    Script Date: 07/13/2009 20:12:11 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuditLog_Segments]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog] DROP CONSTRAINT [FK_AuditLog_Segments]
GO
/****** Object:  Check [CK_AuditLog_ChangeType]    Script Date: 07/13/2009 20:12:11 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_AuditLog_ChangeType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_AuditLog_ChangeType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog] DROP CONSTRAINT [CK_AuditLog_ChangeType]

END
GO
/****** Object:  Table [dbo].[AuditLog]    Script Date: 07/13/2009 20:12:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuditLog]') AND type in (N'U'))
DROP TABLE [dbo].[AuditLog]
GO
/****** Object:  Table [dbo].[Classes]    Script Date: 07/13/2009 20:12:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Classes]') AND type in (N'U'))
DROP TABLE [dbo].[Classes]
GO
/****** Object:  Table [dbo].[Commodities]    Script Date: 07/13/2009 20:12:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Commodities]') AND type in (N'U'))
DROP TABLE [dbo].[Commodities]
GO
/****** Object:  Table [dbo].[Families]    Script Date: 07/13/2009 20:12:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Families]') AND type in (N'U'))
DROP TABLE [dbo].[Families]
GO
/****** Object:  Table [dbo].[Segments]    Script Date: 07/13/2009 20:12:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Segments]') AND type in (N'U'))
DROP TABLE [dbo].[Segments]
GO
/****** Object:  Table [dbo].[Segments]    Script Date: 07/13/2009 20:12:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Segments]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Segments](
	[Key] [numeric](18, 0) NOT NULL,
	[Code] [numeric](8, 0) NOT NULL,
	[Title] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Definition] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Version] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_Segments] PRIMARY KEY CLUSTERED 
(
	[Code] ASC,
	[Version] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Families]    Script Date: 07/13/2009 20:12:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Families]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Families](
	[Key] [numeric](18, 0) NOT NULL,
	[Code] [numeric](8, 0) NOT NULL,
	[Title] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Definition] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Version] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_Families] PRIMARY KEY CLUSTERED 
(
	[Code] ASC,
	[Version] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Commodities]    Script Date: 07/13/2009 20:12:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Commodities]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Commodities](
	[SegmentCode] [numeric](18, 0) NOT NULL,
	[SegmentTitle] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FamilyCode] [numeric](18, 0) NOT NULL,
	[FamilyTitle] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ClassCode] [numeric](18, 0) NOT NULL,
	[ClassTitle] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Key] [numeric](18, 0) NOT NULL,
	[CommodityCode] [numeric](8, 0) NOT NULL,
	[CommodityTitle] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Definition] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Version] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_Commodities] PRIMARY KEY CLUSTERED 
(
	[CommodityCode] ASC,
	[Version] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Classes]    Script Date: 07/13/2009 20:12:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Classes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Classes](
	[Key] [numeric](18, 0) NOT NULL,
	[Code] [numeric](8, 0) NOT NULL,
	[Title] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Definition] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Version] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_Classes] PRIMARY KEY CLUSTERED 
(
	[Code] ASC,
	[Version] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[AuditLog]    Script Date: 07/13/2009 20:12:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuditLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuditLog](
	[effective_version] [numeric](18, 0) NOT NULL,
	[change_type] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[changed_version] [numeric](18, 0) NOT NULL,
	[changed_id] [numeric](18, 0) NULL,
	[changed_code] [numeric](8, 0) NULL,
	[changed_title] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[map_to] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[edit_type] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[move_to] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[effective_id] [numeric](18, 0) NULL,
	[effective_code] [numeric](8, 0) NOT NULL,
	[effective_title] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[effective_date] [datetime] NOT NULL,
	[effective_definition] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[request_id] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'Effective_version'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the UNSPSC code set version in which the change described becomes effective
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'Effective_version'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'change_type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the type of change occurring to the specific code or title in question' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'change_type'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'changed_version'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the UNSPSC code set version that is being modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'changed_version'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'changed_id'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sequentially generated number that is paired with a specific commodity, remaining constant even if the code or title of the commodity changes, identical to effective_key for "edit" and "move" records
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'changed_id'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'changed_code'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the specific code that is being deleted or moved, or the code corresponding to a title that is being edited' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'changed_code'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'changed_title'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the specific title that is being edited, or the title corresponding to the code that is being deleted or moved' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'changed_title'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'map_to'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'indicates that a deleted code has a replacement in the effective UNSPSC code set; represents that the effective_key, effective_code and effective_title are the closest substitute possible for the eliminated code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'map_to'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'edit_type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'indicates the type of change that an edited or moved code is undergoing ("moved" items sometimes have their titles modified slightly)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'edit_type'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'move_to'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'indicates that a moved code has a replacement in the effective UNSPSC code set; represents that the effective_key and effective_title are essentially the same item as the changed_key and changed_title; the moved item has simply been reclassified within the code set' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'move_to'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'effective_id'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sequentially generated number that is paired with a specific commodity, remaining constant even if the code or title is modified in the future, identical to changed_key for "edit" and "move" records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'effective_id'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'effective_code'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the specific code for the new or revised item introduced in the effective UNSPSC code set' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'effective_code'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'effective_title'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The specific title that results from an edit or an add, or the title that corresponds to a moved or deleted code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'effective_title'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'COLUMN',N'effective_date'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the date that a change to the UNSPSC code set is made' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'COLUMN',@level2name=N'effective_date'
GO
/****** Object:  Check [CK_AuditLog_ChangeType]    Script Date: 07/13/2009 20:12:11 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_AuditLog_ChangeType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog]  WITH CHECK ADD  CONSTRAINT [CK_AuditLog_ChangeType] CHECK  (([change_type]='ADD' OR [change_type]='EDIT' OR [change_type]='MOVE' OR [change_type]='DELETE'))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_AuditLog_ChangeType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog] CHECK CONSTRAINT [CK_AuditLog_ChangeType]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuditLog', N'CONSTRAINT',N'CK_AuditLog_ChangeType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Limits the change type to the accepted values

ADD, EDIT, MOVE, DELETE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditLog', @level2type=N'CONSTRAINT',@level2name=N'CK_AuditLog_ChangeType'
GO
/****** Object:  ForeignKey [FK_AuditLog_Classes]    Script Date: 07/13/2009 20:12:11 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuditLog_Classes]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog]  WITH NOCHECK ADD  CONSTRAINT [FK_AuditLog_Classes] FOREIGN KEY([effective_code], [effective_version])
REFERENCES [dbo].[Classes] ([Code], [Version])
NOT FOR REPLICATION
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuditLog_Classes]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog] NOCHECK CONSTRAINT [FK_AuditLog_Classes]
GO
/****** Object:  ForeignKey [FK_AuditLog_Commodities]    Script Date: 07/13/2009 20:12:11 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuditLog_Commodities]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog]  WITH NOCHECK ADD  CONSTRAINT [FK_AuditLog_Commodities] FOREIGN KEY([effective_code], [effective_version])
REFERENCES [dbo].[Commodities] ([CommodityCode], [Version])
NOT FOR REPLICATION
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuditLog_Commodities]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog] NOCHECK CONSTRAINT [FK_AuditLog_Commodities]
GO
/****** Object:  ForeignKey [FK_AuditLog_Families]    Script Date: 07/13/2009 20:12:11 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuditLog_Families]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog]  WITH NOCHECK ADD  CONSTRAINT [FK_AuditLog_Families] FOREIGN KEY([effective_code], [effective_version])
REFERENCES [dbo].[Families] ([Code], [Version])
NOT FOR REPLICATION
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuditLog_Families]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog] NOCHECK CONSTRAINT [FK_AuditLog_Families]
GO
/****** Object:  ForeignKey [FK_AuditLog_Segments]    Script Date: 07/13/2009 20:12:11 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuditLog_Segments]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog]  WITH NOCHECK ADD  CONSTRAINT [FK_AuditLog_Segments] FOREIGN KEY([effective_code], [effective_version])
REFERENCES [dbo].[Segments] ([Code], [Version])
NOT FOR REPLICATION
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuditLog_Segments]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditLog]'))
ALTER TABLE [dbo].[AuditLog] NOCHECK CONSTRAINT [FK_AuditLog_Segments]
GO
