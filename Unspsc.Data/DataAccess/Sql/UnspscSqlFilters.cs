﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnspscSqlFilters.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Contains the UnspscFilters class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc.Data.DataAccess.Sql
{
    using System.Linq;

    /// <summary>
    /// Contains the linq filter extension methods
    /// </summary>
    public static class UnspscSqlFilters
    {
        /// <summary>
        /// With the version.
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of Segment objects</returns>
        public static IQueryable<Segment> WithVersion(this IQueryable<Segment> qry, double version)
        {
            return qry.Where(s => s.Version == (decimal)version);
        }

        /// <summary>
        /// With the segment code.
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="code">The unspsc code.</param>
        /// <param name="version">The version.</param>
        /// <returns>A Segment object</returns>
        public static Segment WithKey(this IQueryable<Segment> qry, UnspscCode code, double version)
        {
            return qry.SingleOrDefault(s => s.Code == (decimal)code.Segment && s.Version == (decimal)version);
        }

        /// <summary>
        /// Filters the families by segment
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="segment">The segment.</param>
        /// <returns>IQueryable of Family objects</returns>
        public static IQueryable<Family> WithSegmentCode(this IQueryable<Family> qry, UnspscCode segment)
        {
            UnspscCode segmentEnd = segment + 1000000d;
            var families = qry.Where(s => s.Code > segment && s.Code < segmentEnd);
            return families;
        }

        /// <summary>
        /// With the version.
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of Family objects</returns>
        public static IQueryable<Family> WithVersion(this IQueryable<Family> qry, double version)
        {
            return qry.Where(s => s.Version == (decimal)version);
        }

        /// <summary>
        /// With the Family code.
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="code">The unspsc code.</param>
        /// <param name="version">The version.</param>
        /// <returns>A Family object</returns>
        public static Family WithKey(this IQueryable<Family> qry, UnspscCode code, double version)
        {
            return qry.SingleOrDefault(s => s.Code == (decimal)code.Segment && s.Version == (decimal)version);
        }

        /// <summary>
        /// Filters the classes by family
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="familyId">The family id.</param>
        /// <returns>IQueryable of IUnspscElement objects</returns>
        public static IQueryable<Class> WithFamilyCode(this IQueryable<Class> qry, UnspscCode familyId)
        {
            UnspscCode familyEnd = familyId + 100000d;
            var classes = qry.Where(f => f.Code > familyId && f.Code < familyEnd);
            return classes;
        }

        /// <summary>
        /// With the version.
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of Class objects</returns>
        public static IQueryable<Class> WithVersion(this IQueryable<Class> qry, double version)
        {
            return qry.Where(s => s.Version == (decimal)version);
        }

        /// <summary>
        /// With the Class code.
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="code">The unspsc code.</param>
        /// <param name="version">The version.</param>
        /// <returns>A Class object</returns>
        public static Class WithKey(this IQueryable<Class> qry, UnspscCode code, double version)
        {
            return qry.SingleOrDefault(s => s.Code == (decimal)code.Segment && s.Version == (decimal)version);
        }

        /// <summary>
        /// Filters the commodities by class
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="classId">The class id.</param>
        /// <returns>IQueryable of Class objects</returns>
        public static IQueryable<Commodity> WithClassCode(this IQueryable<Commodity> qry, UnspscCode classId)
        {
            var commodities = qry.Where(c => c.ClassCode == (decimal)classId.Commodity);
            return commodities;
        }

        /// <summary>
        /// With the version.
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of Class objects</returns>
        public static IQueryable<Commodity> WithVersion(this IQueryable<Commodity> qry, double version)
        {
            return qry.Where(s => s.Version == (decimal)version);
        }

        /// <summary>
        /// With the Commodity code.
        /// </summary>
        /// <param name="qry">The query.</param>
        /// <param name="code">The unspsc code.</param>
        /// <param name="version">The version.</param>
        /// <returns>A Commodity object</returns>
        public static Commodity WithKey(this IQueryable<Commodity> qry, UnspscCode code, double version)
        {
            return qry.SingleOrDefault(s => s.CommodityCode == (decimal)code.Segment && s.Version == (decimal)version);
        }
    }
}