﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUnspscRepository.cs" company="wwwlicious">
//   All rights reserved 2009.
// </copyright>
// <summary>
//   Contains the IUnspscRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Unspsc.Data
{
    using System.Linq;
    using Audit;

    /// <summary>
    /// Provides an interface for a unspsc segment
    /// </summary>
    public interface IUnspscRepository
    {
        /// <summary>
        /// Gets the available versions.
        /// </summary>
        /// <returns>IQueryable of version numbers</returns>
        IQueryable<double> GetVersions();

        /// <summary>
        /// Gets the segments.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of segments</returns>
        IQueryable<IUnspscElement> GetSegments(double version);

        /// <summary>
        /// Gets the families.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of families</returns>
        IQueryable<IUnspscElement> GetFamilies(double version);

        /// <summary>
        /// Gets the families.
        /// </summary>
        /// <param name="segmentCode">The segment code.</param>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of families</returns>
        IQueryable<IUnspscElement> GetFamilies(UnspscCode segmentCode, double version);

        /// <summary>
        /// Gets the classes.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of classes</returns>
        IQueryable<IUnspscElement> GetClasses(double version);

        /// <summary>
        /// Gets the classes.
        /// </summary>
        /// <param name="familyCode">The family code.</param>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of classes</returns>
        IQueryable<IUnspscElement> GetClasses(UnspscCode familyCode, double version);

        /// <summary>
        /// Gets the commodities.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of commodities</returns>
        IQueryable<IUnspscElement> GetCommodities(double version);

        /// <summary>
        /// Gets the commodities.
        /// </summary>
        /// <param name="classCode">The class code.</param>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of commodities</returns>
        IQueryable<IUnspscElement> GetCommodities(UnspscCode classCode, double version);

        /// <summary>
        /// Gets a IUnspscElement.
        /// </summary>
        /// <param name="unspscCode">The unspsc code.</param>
        /// <param name="version">The version.</param>
        /// <returns>IUnspscElement object</returns>
        IUnspscElement GetUnspscElement(UnspscCode unspscCode, double version);

        /// <summary>
        /// Searches commodity titles and descriptions
        /// </summary>
        /// <param name="keywords">The keywords.</param>
        /// <param name="version">The version.</param>
        /// <returns>IQueryable of commodities</returns>
        IQueryable<IUnspscElement> SearchText(string[] keywords, double version);

        /// <summary>
        /// Gets the audit log.
        /// </summary>
        /// <param name="code">The unspsc code.</param>
        /// <returns>IQueryable of IAuditItem</returns>
        IQueryable<IAuditItem> GetAuditLog(UnspscCode code);
    }
}